package com.goeuro.busroute.integration;

import com.goeuro.busroute.rest.service.BusRoutesService;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import static org.junit.Assert.assertEquals;

public class DirectApiTest {

    private HttpServer server;

    private WebTarget target;

    @Before
    public void setUp() throws Exception {

        // start the server
        server = BusRoutesService.startServer();

        // create the client
        Client c = ClientBuilder.newClient();

        target = c.target(BusRoutesService.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testJson() {
        String responseMsg = target.path("direct").queryParam("dep_sid", 20).queryParam("arr_sid", 9).request().get(String.class);
        assertEquals("{\n" +
                "    \"dep_sid\": 20,\n" +
                "    \"arr_sid\": 9,\n" +
                "    \"direct_bus_route\": false\n" +
                "}", responseMsg);
    }
}
