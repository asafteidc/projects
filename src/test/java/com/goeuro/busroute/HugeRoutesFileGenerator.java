package com.goeuro.busroute;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * A generator for huge files containing route definitions.
 * It
 */
public class HugeRoutesFileGenerator {

    private static final int MAX_ROUTES = 100000;

    private static final int MAX_CITIES = 1000000;

    private static final int MAX_CITIES_PER_ROUTE = 1000;

    private static final String FILE_PREFIX = "bus_routes_";


    public static void main(String[] args) {

        File file = null;

        try {
            file = Files.createTempFile(args[0] != null ? args[0] : FILE_PREFIX, ".txt").toFile();
            System.out.println("Creating huge file: " + file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // populate the file, using try-with-resources to make sure we close the file at the end
        try (
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        ) {

            bw.write("10000");
            bw.newLine();

            Random random = new Random();

            for (int i = 1; i <= MAX_ROUTES; i++) {
                Set<Integer> cities = new HashSet<>();
                StringBuffer buffer = new StringBuffer();
                buffer.append(i).append(" ");
                for (int j = 0; j < MAX_CITIES_PER_ROUTE; j++) {
                    int next;

                    // make sure a route does not go twice through the same city
                    while (cities.contains(next = 1 + random.nextInt(MAX_CITIES + 1))) ;
                    cities.add(next);
                    buffer.append(next).append(" ");
                }
                bw.write(buffer.toString());
                bw.newLine();
                bw.flush();
            }

            bw.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
