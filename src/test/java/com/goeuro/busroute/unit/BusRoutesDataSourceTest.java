package com.goeuro.busroute.unit;

import com.goeuro.busroute.model.BusRoutesDataSource;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BusRoutesDataSourceTest {

    private BusRoutesDataSource busRoutesDataSource = BusRoutesDataSource.getInstance();


    @After
    public void tearDown() {
        busRoutesDataSource.clear();
    }


    @Test
    public void testUnknownCity() {

        busRoutesDataSource.onRoute(new String[]{"1", "1", "2"});

        assertEquals(false, busRoutesDataSource.isDirectRoute(1, 3));
        assertEquals(false, busRoutesDataSource.isDirectRoute(2, 3));
        assertEquals(false, busRoutesDataSource.isDirectRoute(3, 4));

    }

    @Test
    public void testSameCity() {

        busRoutesDataSource.onRoute(new String[]{"1", "1", "2"});

        assertEquals(true, busRoutesDataSource.isDirectRoute(1, 1));
        assertEquals(true, busRoutesDataSource.isDirectRoute(2, 2));
        assertEquals(false, busRoutesDataSource.isDirectRoute(3, 3));

    }

    @Test
    public void testDirection() {

        busRoutesDataSource.onRoute(new String[]{"1", "2", "3"});

        assertEquals(true, busRoutesDataSource.isDirectRoute(2, 3));
        assertEquals(false, busRoutesDataSource.isDirectRoute(3, 2));

        busRoutesDataSource.onRoute(new String[]{"2", "3", "2"});

        assertEquals(true, busRoutesDataSource.isDirectRoute(2, 3));
        assertEquals(true, busRoutesDataSource.isDirectRoute(3, 2));

    }

    @Test
    public void testNonTransitivity() {

        busRoutesDataSource.onRoute(new String[]{"1", "1", "2"});
        busRoutesDataSource.onRoute(new String[]{"2", "2", "3"});

        assertEquals(false, busRoutesDataSource.isDirectRoute(1, 3));

    }

    @Test
    public void test3() {

        String[][] routes = new String[][]{
                new String[]{"1", "153", "150", "148", "106", "17", "20", "160", "140", "24"},
                new String[]{"2", "5", "142", "106", "11"},
                new String[]{"19", "153", "121", "114", "150", "5"},
                new String[]{"13", "153", "148", "169", "106", "11", "12"},
                new String[]{"14", "114", "150", "142", "12", "179", "174", "17"},
                new String[]{"6", "5", "138", "148", "12", "174", "118", "16", "19", "184"},
                new String[]{"7", "121", "114", "150", "5", "148", "169", "11"},
                new String[]{"8", "142", "138", "148", "169", "106", "11", "12"},
                new String[]{"18", "153", "114", "5", "138"},
                new String[]{"11", "121", "114", "148", "169", "12", "16", "155"}
        };

        busRoutesDataSource.onRoute(routes);

        assertEquals(false, busRoutesDataSource.isDirectRoute(3, 4));
        assertEquals(false, busRoutesDataSource.isDirectRoute(0, 1));
        assertEquals(true, busRoutesDataSource.isDirectRoute(153, 5));
        assertEquals(false, busRoutesDataSource.isDirectRoute(153, 16));
    }

}
