package com.goeuro.busroute.rest.service;

import com.goeuro.busroute.model.BusRoutesDataSource;
import com.goeuro.busroute.model.RoutesLoader;
import com.goeuro.busroute.model.RoutesLodingException;
import org.glassfish.grizzly.Grizzly;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * BusRoutesService entry point.
 */
public class BusRoutesService {

    public static final String BASE_URI = "http://localhost:8088/api/";

    private static final Logger LOGGER = Grizzly.logger(BusRoutesService.class);

    /**
     * Starts the Grizzly HTTP server.
     *
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {

        // create a resource config that scans for JAX-RS resources and providers
        final ResourceConfig rc = new ResourceConfig().packages("com.goeuro.busroute");

        // create and start a new instance of grizzly http server exposing the application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Failed to start application, wrong number of arguments");
            }
            System.err.println("Wrong number of arguments, expected 1 param, the routes file path.");
            System.exit(1);
        }

        RoutesLoader loader = new RoutesLoader();
        loader.registerListener(BusRoutesDataSource.getInstance());

        try {
            loader.load(args[0]);
        } catch (RoutesLodingException e) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Failed to load routes data", e);
            }
            e.printStackTrace();
            System.exit(1);
        }

        startServer();
    }
}

