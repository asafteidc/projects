package com.goeuro.busroute.rest.api;

import com.goeuro.busroute.model.BusRoutesDataSource;
import com.goeuro.busroute.model.Response;
import org.glassfish.grizzly.Grizzly;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("direct")
public class DirectApi {


    private static final Logger LOGGER = Grizzly.logger(DirectApi.class);

    /**
     * Method handling HTTP GET requests.
     *
     * @return String that will be returned as a json response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String isDirectRoute(@QueryParam("dep_sid") int depSid, @QueryParam("arr_sid") int arrSid) {

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "new request for [dep_sid=" + depSid + ", arr_sid=" + arrSid + "]");
        }

        boolean found = BusRoutesDataSource.getInstance().isDirectRoute(depSid, arrSid);

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, found ? "found" : "not found" + " direct route for [dep_sid=" + depSid + ", arr_sid=" + arrSid + "]");
        }

        return new Response(depSid, arrSid, found).toString();
    }

}
