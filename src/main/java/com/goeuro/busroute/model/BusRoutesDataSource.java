package com.goeuro.busroute.model;

import org.glassfish.grizzly.Grizzly;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The source of bus routes information.
 * Implements the Singleton pattern.
 * Specifically created to easily query whether 2 cities are connected by a direct route.
 * It is unnecessary for this class to be thread safe, as for our micro-service we first load all routes within it
 * and only then allow threads to query for direct routes existence.
 */
public class BusRoutesDataSource implements RouteListener {

    private static final Logger LOGGER = Grizzly.logger(BusRoutesDataSource.class);

    private static final BusRoutesDataSource instance = new BusRoutesDataSource();

    // every city id is associated all routes it belongs to;
    // together with the route id, the actual position on that route is also stored;
    private final Map<Integer, Map<Integer, Integer>> citiesToRoutesMap = new HashMap<>();

    private BusRoutesDataSource() {
    }

    public static BusRoutesDataSource getInstance() {
        return instance;
    }

    /**
     * Called on routes load time
     *
     * @param routeTokens the route details to be loaded
     */
    @Override
    public void onRoute(String[] routeTokens) {

        if (LOGGER.isLoggable(Level.CONFIG)) {
            LOGGER.log(Level.CONFIG, "Loading route: " + Arrays.toString(routeTokens));
        }

        int routeId = Integer.parseInt(routeTokens[0]);

        for (int i = 1; i < routeTokens.length; i++) {

            int cityId = Integer.parseInt(routeTokens[i]);

            Map<Integer, Integer> routes = citiesToRoutesMap.get(cityId);

            if (routes == null) {
                routes = new HashMap<>();
                citiesToRoutesMap.put(cityId, routes);
            }

            routes.put(routeId, i);
        }

    }

    @Override
    public void onRoute(String[][] routes) {

        for (String[] route : routes) onRoute(route);
    }

    /**
     * Whether there is a direct route from depSid to arrSid.
     *
     * @param depSid departure city id
     * @param arrSid arrival city id
     * @return true if there is a direct route from depSid to arrSid, false otherwise
     */
    public boolean isDirectRoute(int depSid, int arrSid) {

        // find all routes the departure city belongs to
        Map<Integer, Integer> routesThroughDep = citiesToRoutesMap.get(depSid);
        if (routesThroughDep == null) {
            return false;
        }

        if (depSid == arrSid) return true;

        // find all routes the arrival city belongs to
        Map<Integer, Integer> routesThroughArr = citiesToRoutesMap.get(arrSid);

        if (routesThroughArr == null) {
            return false;
        }

        // find all routes in common for the 2 cities
        Set<Integer> commonRoutes = getIntersection(routesThroughDep, routesThroughArr);


        // iterate through all common routes and detect whether there is one
        // such that departure city is before arrival city
        boolean found = false;
        for (Integer r : commonRoutes) {
            Integer position1 = routesThroughDep.get(r);
            Integer position2 = routesThroughArr.get(r);
            if (position1 < position2) {
                // found a route, so break the iteration
                found = true;
                break;
            }
        }

        return found;
    }

    public void clear() {
        this.citiesToRoutesMap.clear();
    }

    private Set<Integer> getIntersection(Map<Integer, Integer> map1, Map<Integer, Integer> map2) {

        Set<Integer> tSet1 = new HashSet<>();
        tSet1.addAll(map1.keySet());

        Set<Integer> tSet2 = new HashSet<>();
        tSet2.addAll(map2.keySet());

        tSet1.retainAll(tSet2);

        return tSet1;
    }

}
