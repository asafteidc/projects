package com.goeuro.busroute.model;

public class Response {

    private int depSid;

    private int arrSid;

    private boolean directBusRoute;

    public Response(int depSid, int arrSid, boolean directBusRoute) {
        this.depSid = depSid;
        this.arrSid = arrSid;
        this.directBusRoute = directBusRoute;
    }

    @Override
    public String toString() {

        return "{\n" +
                "    \"dep_sid\": " + depSid + ",\n" +
                "    \"arr_sid\": " + arrSid + ",\n" +
                "    \"direct_bus_route\": " + directBusRoute + "\n" +
                "}";
    }

}
