package com.goeuro.busroute.model;

public class RoutesLodingException extends Exception {

    public RoutesLodingException(String message) {
        super(message);
    }

    public RoutesLodingException(String message, Throwable cause) {
        super(message, cause);
    }
}
