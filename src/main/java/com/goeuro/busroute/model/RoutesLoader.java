package com.goeuro.busroute.model;

import org.glassfish.grizzly.Grizzly;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Responsible with the process of loading the routes data from disk.
 * Any registered listener will be notified for every single route definition found.
 */
public class RoutesLoader {

    private static final Logger LOGGER = Grizzly.logger(RoutesLoader.class);

    private final List<RouteListener> listeners = new ArrayList<>();

    public RoutesLoader registerListener(RouteListener listener) {
        this.listeners.add(listener);
        return this;
    }

    /**
     * Loads the routes from the provided file path into memory.
     * It reads the file line by line, 1st one telling the number of remaining lines to be processed.
     *
     * @param path path of the file to be loaded
     * @throws RoutesLodingException in case something goes wrong during loading
     */
    public void load(String path) throws RoutesLodingException {

        if (LOGGER.isLoggable(Level.CONFIG)) {
            LOGGER.log(Level.CONFIG, "Loading routes from file path " + path + " ...");
        }

        try (
                BufferedReader bf = new BufferedReader(new FileReader(path))
        ) {

            // 1st line tells how many routes are to be loaded further
            int routes = Integer.parseInt(bf.readLine().trim());

            String line;
            int routesProcessed = 0;

            // read max @routes lines as long as end of stream is not reached
            while (routesProcessed < routes && (line = bf.readLine()) != null) {
                processLine(line);
                routesProcessed++;
            }

        } catch (Exception e) {
            throw new RoutesLodingException("Failed loading routes", e);
        }

    }

    /**
     * Processes a line by splitting into tokens separated by spaces.
     * And notifies all registered listeners.
     *
     * @param line the line to be processed
     */
    private void processLine(String line) throws RoutesLodingException{
        String[] tokens = line.split("\\s");
        if (tokens.length < 3) {
            throw new RoutesLodingException("Less than 3 tokens on route:" + line);
        }
        notifyListeners(tokens);
    }

    private void notifyListeners(String[] tokens) {
        listeners.forEach((l) -> l.onRoute(tokens));
    }

}
