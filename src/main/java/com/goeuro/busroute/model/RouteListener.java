package com.goeuro.busroute.model;

public interface RouteListener {

    void onRoute(String[] routeTokens);

    void onRoute(String[][] routes);
}
